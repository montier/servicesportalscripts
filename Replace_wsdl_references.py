#   
# 	Licensed to MonTier Software (2015) LTD under one or more contributor
#   license agreements. See the NOTICE file distributed with
#   this work for additional information regarding copyright
#   ownership. MonTier Software (2015) LTD licenses this file to you under
#   the Apache License, Version 2.0 (the "License"); you may
#   not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#  
#      http://www.apache.org/licenses/LICENSE-2.0
#  
#   Unless required by applicable law or agreed to in writing,
#   software distributed under the License is distributed on an
#   "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
#   KIND, either express or implied.  See the License for the
#   specific language governing permissions and limitations
#   under the License.
#
 

# The scripts reads an original WSDL files, and a new updated files of the same WSDL(names can be different)
# The script will alter the updates files to have correct refernences as the original ones so there won't be
# any broken refernences when uploading them to datapower

import ntpath
import sys
import collections.abc
import os
import random
from lxml import etree

from DPMapping import DPMapping

class Vars:
    error_message = ""
    exitValue = 0

# Returns the referenced import file path of an import element
def getReferencedXsdFromImportElement( xmlElement ):
    nameSpaceTag = xmlElement.tag 
    if nameSpaceTag == '{http://www.w3.org/2001/XMLSchema}import':
        if 'schemaLocation' in xmlElement.attrib :
            return xmlElement.attrib['schemaLocation']
        if 'namespace' in xmlElement.attrib :
            return xmlElement.attrib['namespace']
        print(xmlElement)
        Vars.error_message = "XSD import didn't contain required attributes for import(namespace / schemaLocation)"
        Vars.exitValue = 99
        
    if nameSpaceTag == '{http://www.w3.org/2001/XMLSchema}include':
        if 'schemaLocation' in xmlElement.attrib :
            return xmlElement.attrib['schemaLocation']
        print(xmlElement)
        Vars.error_message = "XSD iclude import didn't contain required attributes for import(schemaLocation)"
        Vars.exitValue = 99
    if nameSpaceTag == '{http://schemas.xmlsoap.org/wsdl/}import':
        if 'location' in xmlElement.attrib :
            return xmlElement.attrib['location']
        if 'namespace' in xmlElement.attrib :
            return xmlElement.attrib['namespace']
        Vars.error_message = "WSDL import didn't contain required attributes for import(namespace / location)"
        Vars.exitValue = 99

    return ''
    
# Returns a mapping of namespaces and their following XSD files in an XML
# If there's an import\include reference, they will be processed and mapped as well
def mapReferenceAndNamespaceHierarchyOfWSDLFile( XMLFilePath,overridedNamespace='', overridedlocalFilePath='',referencedBy=''):
    "Returns a mapping of namespaces and their following XSD files in an XML"

    # If an error already occurred during scan stop scanning
    if Vars.exitValue == 99 :
        return []

    if XMLFilePath.startswith('http://')  or XMLFilePath.startswith('https://') or XMLFilePath.startswith('ftp://') or XMLFilePath.startswith('sftp://') or XMLFilePath.startswith('ftps://'):
        mapReferenceHierarchyOfWSDLFile.Vars.error_message = "Remote file references are not supported by this job"
        sys.exit(99)
        
    # Read the file form the whole path
    filename = ntpath.basename(XMLFilePath);

    # Check if path contain no file name
    if not filename:
        Vars.error_message = 'XML File path has empty file name ' + XMLFilePath
        Vars.exitValue = 99
        return XMLFilePath

    # This file name was already scanned, reference loop detected
    if XMLFilePath in mapReferenceAndNamespaceHierarchyOfWSDLFile.scannedFiles:
        return []
    mapReferenceAndNamespaceHierarchyOfWSDLFile.scannedFiles.append(XMLFilePath)

    try:
        namespaceMapping = []
        fileToRead = XMLFilePath
        # Parse the WSDL\XSD File
        if overridedlocalFilePath != '':
            fileToRead = overridedlocalFilePath

        xmlContent = etree.parse(config["original_files_path"] + '//' + fileToRead).getroot()
        nameSpace = overridedNamespace;
        if nameSpace == '':
            nameSpace = xmlContent.attrib['targetNamespace']
        if not nameSpace:
            Vars.error_message = 'XSD has no declared \'targetNamespace\' ' + XMLFilePath
            Vars.exitValue = 99
        
        # Use overrided local path for mapping if function received it
        if overridedlocalFilePath != '':
            if '///' in overridedlocalFilePath:
                localPath = overridedlocalFilePath.rsplit('///',1)[1]
            else:
                localPath = overridedlocalFilePath
        else:
            localPath = findXSDFileNameInOriginalConfigByPath(XMLFilePath)
        
        namespaceMapping.append(DPMapping(referencedBy, XMLFilePath, localPath, nameSpace))
        
        # Find all includes
        for includeElement in xmlContent.iter('{http://www.w3.org/2001/XMLSchema}include'):
            foundFileName = getReferencedXsdFromImportElement(includeElement)
            # Ignore remote file referencing
            if foundFileName.startswith('http') :
                continue
                    
            mappingExist = False
            # Check if mapping already exist by namespace and name for include
            for mapping in namespaceMapping:
                if mapping.nameSpace == nameSpace and mapping.dpPath == foundFileName:
                    mappingExist = True
            if not mappingExist:
                localFoundFile = findXSDFileNameInOriginalConfigByPath(foundFileName)
                # When running the script all mapping should be valid if there's no mapping between dp path to local path, stop the script
                if localFoundFile == '':
                    Vars.error_message = 'Mapping not found(in input file) for ' + foundFileName + ' referenced by ' + XMLFilePath
                    Vars.exitValue = 99
                    return []
                    

                
                # The mapping points to a non existing file, stop the script
                if not os.path.isfile(config["original_files_path"] + '//' + localFoundFile):
                    Vars.error_message = 'File not found on local file system: ' + localFoundFile + ' referenced by ' + XMLFilePath
                    Vars.exitValue = 99
                    return []
                dpPath = foundFileName
                if not 'local:///' in dpPath:
                    wsdlDirNameInDatapower = config["original_start_wsdl_dp_path"].rsplit('/',1)[0] + '/'
                    dpPath = wsdlDirNameInDatapower + dpPath
                namespaceMapping.append(DPMapping(XMLFilePath ,dpPath, localFoundFile, nameSpace))
                
        
        # Find all xsd import
        for importXsdElement in xmlContent.iter('{http://www.w3.org/2001/XMLSchema}import'):
            foundFileName = getReferencedXsdFromImportElement(importXsdElement)
            # Ignore remote file referencing
            if foundFileName.startswith('http') :
                continue
                
            localFileName = findXSDFileNameInOriginalConfigByPath(foundFileName)
            if localFileName == '':
                Vars.error_message = 'Mapping(in input file) not found for ' + foundFileName + ' referenced by ' + XMLFilePath
                Vars.exitValue = 99
                return[]
            
            
            try:
                newNameSpace = importXsdElement.attrib['namespace']
            except KeyError:
                # Do nothing, no namespace declared in import
                newNameSpace = ''
            dpPath = foundFileName
            if not 'local:///' in dpPath:
                wsdlDirNameInDatapower = config["original_start_wsdl_dp_path"].rsplit('/',1)[0] + '/'
                dpPath = wsdlDirNameInDatapower + dpPath
            namespaceMapping.extend(mapReferenceAndNamespaceHierarchyOfWSDLFile(dpPath, newNameSpace, localFileName, XMLFilePath))
        
        # Find all wsdl import
        for importWsdlElement in xmlContent.iter('{http://schemas.xmlsoap.org/wsdl/}import'):
            foundFileName = getReferencedXsdFromImportElement(importWsdlElement)
            # Ignore remote file referencing
            if foundFileName.startswith('http') :
                continue
                
            localFileName = findXSDFileNameInOriginalConfigByPath(foundFileName)
            if localFileName == '':
                Vars.error_message = 'Mapping(in input file) not found for ' + foundFileName + ' referenced by ' + XMLFilePath
                Vars.exitValue = 99
                return[]

            try:
                newNameSpace = importWsdlElement.attrib['namespace']
            except KeyError:
                # Do nothing, no namespace declared in import
                newNameSpace = ''
            dpPath = foundFileName
            if not 'local:///' in dpPath:
                wsdlDirNameInDatapower = config["original_start_wsdl_dp_path"].rsplit('/',1)[0] + '/'
                dpPath = wsdlDirNameInDatapower + dpPath
            namespaceMapping.extend(mapReferenceAndNamespaceHierarchyOfWSDLFile(dpPath, newNameSpace, localFileName, XMLFilePath))

        if Vars.exitValue > 90 :
            return []
        return namespaceMapping

    except IOError as notFoundError:
        # Couldn't parse the WSDL, either non xml or file doesnt exist
        Vars.error_message = 'Couldn\'t find file ' + fileToRead
        Vars.exitValue = 99
        return []

    except etree.ParseError as parseError:
        # The referenced file was found but is invalid XML
        # Exit app
        Vars.error_message = XMLFilePath + ' is invalid XML file, at line %s column %s' % (parseError.position)
        Vars.exitValue = 99

mapReferenceAndNamespaceHierarchyOfWSDLFile.scannedFiles = []


# Scans a given wsdl file, verifies all referenced files exist and make a mapping of files to their namespaces
def createNameSpaceMappingForNewWSDL( WSDLFilePath ):
    # If an error already occurred during scan stop scanning
    if Vars.exitValue == 99 :
        return []

    if WSDLFilePath.startswith('http://') or WSDLFilePath.startswith('https://') or WSDLFilePath.startswith('ftp://') or WSDLFilePath.startswith('sftp://') or WSDLFilePath.startswith('ftps://'):
        Vars.error_message = "Remote file references are not supported by this job"
        sys.exit(99)

    # Read the file form the whole path
    filename = ntpath.basename(WSDLFilePath);

    # Check if path contain no file name
    if not filename:
        Vars.error_message = 'XML File path has empty file name ' + WSDLFilePath
        Vars.exitValue = 99
        return WSDLFilePath

    try:
        namespaceMapping = []
        processedFile = WSDLFilePath
        xmlContent = etree.parse(config["new_files_path"] + '//' + 	processedFile).getroot()
        if not 'targetNamespace' in xmlContent.attrib:
            Vars.error_message = 'XSD has no declared \'targetNamespace\' ' + processedFile
            Vars.exitValue = 99
            return []

        nameSpace = xmlContent.attrib['targetNamespace']
        namespaceMapping.append(DPMapping('','',processedFile,nameSpace))
        newMappedXsds = int(config["total_new_schema_files"])
        for n in range(0,newMappedXsds):
            processedFile = config["new_schema_file.%d.name" % (n)]
            xmlContent = etree.parse(config["new_files_path"] + '//' + 	processedFile).getroot()
            if not 'targetNamespace' in xmlContent.attrib:
                Vars.error_message = 'XSD has no declared \'targetNamespace\' ' + processedFile
                Vars.exitValue = 99
                return []

            nameSpace = xmlContent.attrib['targetNamespace']
            namespaceMapping.append(DPMapping('','',processedFile,nameSpace))

        return namespaceMapping
    
    except IOError as notFoundError:
        # Couldn't parse the WSDL, either non xml or file doesnt exist
        Vars.error_message = 'Couldn\'t find file ' + processedFile
        Vars.exitValue = 99
        return []
    
    except etree.ParseError as parseError:
        # The referenced file was found but is invalid XML
        # Exit app
        Vars.error_message = processedFile + ' is invalid XML file, at line %s column %s' % (parseError.position)
        Vars.exitValue = 99
        return []
        
createNameSpaceMappingForNewWSDL.scannedFiles = []
    

def findXSDFileNameInOriginalConfigByPath(pathToFind):
    wsdlDirNameInDatapower = config["original_start_wsdl_dp_path"].rsplit('/',1)[0] + '/'
    pathToFind = pathToFind.replace(wsdlDirNameInDatapower,'')
    mappedXsd = int(config["total_original_schema_files"])
    for n in range(0,mappedXsd):
        dppath = config["original_schema_file.%d.dp_path" % (n)]
        # Remove WSDL base dir path
        dppath = dppath.replace(wsdlDirNameInDatapower,'')
        if dppath == pathToFind:
            path = config["original_schema_file.%d.name" % (n)]
            return path
    return ''
    
# Loads a properties file into dictionary
def load_properties(filepath, sep='=', comment_char='#'):
    """
    Read the file passed as parameter as a properties file.
    """
    props = {}
    with open(filepath, "rt") as f:
        for line in f:
            l = line.strip()
            if l and not l.startswith(comment_char):
                key_value = l.split(sep)
                key = key_value[0].strip()
                value = sep.join(key_value[1:]).strip().strip('"') 
                props[key] = value 
    return props

# Finds a matching namespace and filename in the DP Original WSDL mapping
# if it is found, use this path to override the XSD in DP
# if it is not found, will put the file next to the wsdl
def findMatchedMapping(dpMappingCollection, foundFileName , nameSpace):
    for mapping in dpMappingCollection:
        if mapping.dpPath == foundFileName and mapping.nameSpace == nameSpace:
            return mapping
    # If no mapping was found,use WSDL folder to store this XSD
    wsdlDirNameInDatapower = config["original_start_wsdl_dp_path"].rsplit('/',1)[0] + '/'
    rndNum = str(random.randint(1, 1000))
    
    # If not found original to override, before generate mapping make sure that there's no mapping already one generated that matches
    for mapping in findMatchedMapping.newGeneratedMapping:
        # Before comparing, just extract the full file name
        dpMapToCompare = foundFileName.replace(config["original_start_wsdl_dp_path"].rsplit('/',1)[0] + '/','')

        # Expect the random file name to end with the original file name
        if mapping.dpPath.endswith(dpMapToCompare) and mapping.nameSpace == nameSpace:
            return mapping
    
    mapping = DPMapping('', wsdlDirNameInDatapower +  rndNum + '_' + foundFileName.replace(wsdlDirNameInDatapower,''), foundFileName ,nameSpace)

    # Save the created mapping
    findMatchedMapping.newGeneratedMapping.append(mapping)
        
    return mapping
findMatchedMapping.newGeneratedMapping = []
    
# The input file should be in the same directory as the script
config = load_properties('input.props')

# Load and parse WSDL file, and make a tree out of the referenced fiels
originalNamespaceMapping = mapReferenceAndNamespaceHierarchyOfWSDLFile(config["original_start_wsdl_dp_path"],'',config["original_start_wsdl_name"],config["original_start_wsdl_dp_path"])

newXSDsScanOutput = createNameSpaceMappingForNewWSDL(config["new_start_wsdl_name"])

newXsdMapping = ''

wsdlDirNameInDatapower = config["original_start_wsdl_dp_path"].rsplit('/',1)[0] + '/'
# Read the new XSDs files and alter them
for n in range(0,len(newXSDsScanOutput)):
    processedFileName = newXSDsScanOutput[n].localPath
    xmlContent = etree.parse(config["new_files_path"] + '//' + 	processedFileName).getroot()
    
    # Find all includes
    for includeElement in xmlContent.iter('{http://www.w3.org/2001/XMLSchema}include'):
        foundFileName = getReferencedXsdFromImportElement(includeElement)
        
        # Ignore remote file referencing
        if foundFileName.startswith('http') :
            continue
                
        if not wsdlDirNameInDatapower in foundFileName:
            foundFileName = wsdlDirNameInDatapower + foundFileName
        # Find the dp mapping from original that best fits the currently parsed new xsd file
        dpMapping = findMatchedMapping(originalNamespaceMapping,foundFileName,newXSDsScanOutput[n].nameSpace)
        
        # The import should be relative, remove start of prefix
        includeElement.attrib['schemaLocation']  = dpMapping.dpPath.replace(wsdlDirNameInDatapower,'')
    
    # Find all xsd imports
    for importXsdElement in xmlContent.iter('{http://www.w3.org/2001/XMLSchema}import'):
        foundFileName = getReferencedXsdFromImportElement(importXsdElement)
        
        # Ignore remote file referencing
        if foundFileName.startswith('http') :
            continue
            
        if foundFileName == '':
            Vars.error_message = 'No schema location in import element in file ' + foundFileName
            Vars.exitValue = 99
            
        # Find the namespace of the import
        try:
            innerNameSpace = importXsdElement.attrib['namespace']
        except KeyError:
            innerXMLContent = etree.parse(config["new_files_path"] + '//' + foundFileName).getroot()
            innerNameSpace = innerXMLContent.attrib['targetNamespace']
        
        if not wsdlDirNameInDatapower in foundFileName:
            foundFileName = wsdlDirNameInDatapower + foundFileName
        dpMapping = findMatchedMapping(originalNamespaceMapping,foundFileName,innerNameSpace)

        # The import should be relative, remove start of prefix
        importXsdElement.attrib['schemaLocation'] = dpMapping.dpPath.replace(wsdlDirNameInDatapower,'')

    for importWsdlElement in xmlContent.iter('{http://schemas.xmlsoap.org/wsdl/}import'):
        foundFileName = getReferencedXsdFromImportElement(importWsdlElement)
        
        # Ignore remote file referencing
        if foundFileName.startswith('http') :
            continue
        
        if foundFileName == '':
            Vars.error_message = 'No schema location in import element in file ' + foundFileName
            Vars.exitValue = 99
            
        # Find the namespace of the import
        try:
            innerNameSpace = importWsdlElement.attrib['namespace']
        except KeyError:
            innerXMLContent = etree.parse(config["new_files_path"] + '//' + foundFileName).getroot()
            innerNameSpace = innerXMLContent.attrib['targetNamespace']
        
        if not wsdlDirNameInDatapower in foundFileName:
            foundFileName = wsdlDirNameInDatapower + foundFileName
        dpMapping = findMatchedMapping(originalNamespaceMapping,foundFileName,innerNameSpace)
        
        # The import should be relative, remove start of prefix
        importWsdlElement.attrib['location'] = dpMapping.dpPath.replace(wsdlDirNameInDatapower,'')
        
    if not wsdlDirNameInDatapower in processedFileName:
        processedFileName = wsdlDirNameInDatapower + processedFileName
    currentMapping = findMatchedMapping(originalNamespaceMapping,processedFileName,xmlContent.attrib['targetNamespace'])

    f = open('altered/%s'%(ntpath.basename(newXSDsScanOutput[n].localPath)), 'w')
    f.write('<?xml version="1.0" encoding="utf-8" ?>\n'+etree.tostring(xmlContent, encoding=str))
    
    f.close()
    # First processed file is the WSDL, therefore it has a different property name in the file
    if n == 0:
        newXsdMapping = newXsdMapping + 'altered_wsdl_file.name=%s\naltered_wsdl_file.dp_path=%s\n' % (newXSDsScanOutput[n].localPath , currentMapping.dpPath)
    else:
        newXsdMapping = newXsdMapping + 'altered_schema_file.%d.name=%s\naltered_schema_file.%d.dp_path=%s\n' % (n - 1, newXSDsScanOutput[n].localPath , n - 1, currentMapping.dpPath)
    
# Build the payload
payload = "completion_code=%d\nerror_message=%s\n%s" % (Vars.exitValue, Vars.error_message, newXsdMapping)
print('') 
print('------------------Output file Payload----------------------')
print(payload)
# Save the payload into file so DPOD can read the output
f = open('completion-%s.props'%(config['iteration']), 'w')
f.write(payload)
f.close()
sys.exit(Vars.exitValue)

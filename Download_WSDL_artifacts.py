#   
# 	Licensed to MonTier Software (2015) LTD under one or more contributor
#   license agreements. See the NOTICE file distributed with
#   this work for additional information regarding copyright
#   ownership. MonTier Software (2015) LTD licenses this file to you under
#   the Apache License, Version 2.0 (the "License"); you may
#   not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#  
#      http://www.apache.org/licenses/LICENSE-2.0
#  
#   Unless required by applicable law or agreed to in writing,
#   software distributed under the License is distributed on an
#   "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
#   KIND, either express or implied.  See the License for the
#   specific language governing permissions and limitations
#   under the License.
#
 

# Finds XSDs referenced by a given WSDL file that are missing or invalid in the local filesystem.
# For example, if WSDL contains the following line 
# <xsd:import schemaLocation="delta/import.xsd" />
# expects to find the file 'import.xsd' in [folder of the script]/new.
# If the file is not found / invalid XML, the script will return it as part of the list of missing files and exit with value 1
# If all files found, will exit with 0
# The script runs using an input file in the following format
    #request_uuid=97C1AD50-97EF-488C-AEA8-5BCFB1D2823A
    #call_timestamp=1506592486
    #calling_user_name=admin
    #script_name=script1.py
    #iteration=2
    #invoke_type=REST  #invokeType = UI ot REST
    #requested_operation=validate (or promote)
    #service_name=myService.wsp
    #device_name=myDevice
    #domain_name=myDomain
    #log_files_path=/jobpath/logs 
    #original_files_path=C:\Users\User\Desktop\WSDLTransformationAndValidation\new
    #total_original_schema_files=3
    #original_start_wsdl_dp_path=local:///references.wsdl 
    #original_start_wsdl_name=references.wsdl
    #original_schema_file.0.dp_path=import.xsd 
    #original_schema_file.0.name=import.xsd 
    #original_schema_file.1.dp_path=local:///include.xsd 
    #original_schema_file.1.name=include1.xsd
    #original_schema_file.2.dp_path=local:///lel.xsd 
    #original_schema_file.2.name=lel.xsd
# The scripts uses the mapping in the file (dp_path -> name) to find XSDs
# Eg. WSDL contains line <xsd:import schemaLocation="local:///include.xsd" />.
# Script will find the mapping original_schema_file.1.dp_path=local:///include.xsd and will try to find the actual file on filesystem by:
# original_schema_file.1.name=include1.xsd

import xml.etree.ElementTree
import ntpath
import sys
import collections.abc
import os

class Vars:
    error_message = ""
    exitValue = 0

# Returns the referenced import file path of an import element
def getReferencedXsdFromImportElement( xmlElement ):
    nameSpaceTag = xmlElement.tag 
    
    if nameSpaceTag == '{http://www.w3.org/2001/XMLSchema}import':
        if 'schemaLocation' in xmlElement.attrib :
            return xmlElement.attrib['schemaLocation']
        if 'namespace' in xmlElement.attrib :
            return xmlElement.attrib['namespace']
        print(xmlElement)
        Vars.error_message = "XSD import didn't contain required attributes for import(namespace / schemaLocation)"
        Vars.exitValue = 99
        
    if nameSpaceTag == '{http://www.w3.org/2001/XMLSchema}include':
        if 'schemaLocation' in xmlElement.attrib :
            return xmlElement.attrib['schemaLocation']
        print(xmlElement)
        Vars.error_message = "XSD iclude import didn't contain required attributes for import(schemaLocation)"
        Vars.exitValue = 99
    
    if nameSpaceTag == '{http://schemas.xmlsoap.org/wsdl/}import':
        if 'location' in xmlElement.attrib :
            return xmlElement.attrib['location']
        if 'namespace' in xmlElement.attrib :
            return xmlElement.attrib['namespace']
        Vars.error_message = "WSDL import didn't contain required attributes for import(namespace / location)"
        Vars.exitValue = 99
    
    return ''
    
    
# Returns tree of referenced XSD/WSDL files
# A parent node linked to nodes which he is referenced to in XML
# If referenced XSD file doesn't exist, it will be a leaf in the tree with no children nodes
# Eg. ['import.xsd']
# However, if the XSD exist but doesnt reference anything, it will look have empty list of children nodes
# Eg. ['import.xsd', []]
def mapReferenceHierarchyOfWSDLFile( XMLFilePath ):
    "Returns tree of referenced XSD/WSDL files"
    
    # If an error already occurred during scan stop scanning
    if Vars.exitValue > 90 :
        return []
        
    # Remove filesystem prefix of datapower
    # Eg. local:///file.wsdl -> file.wsdl
    if '///' in XMLFilePath:
        XMLFilePath = XMLFilePath.rsplit('///',1)[1]
    # Read the file form the whole path
    filename = ntpath.basename(XMLFilePath);

    # Check if path contain no file name
    if not filename:
        Vars.error_message = 'XML File path has empty file name ' + XMLFilePath
        Vars.exitValue = 99
        return XMLFilePath

    # This file name was already scanned, reference loop detected
    if XMLFilePath in mapReferenceHierarchyOfWSDLFile.scannedFiles:
        return []
    mapReferenceHierarchyOfWSDLFile.scannedFiles.append(XMLFilePath)

    try:
        schemaList = []
        # Parse the WSDL\XSD File
        wsdlContent = xml.etree.ElementTree.parse(config["original_files_path"] + '//' + XMLFilePath).getroot()
        
        # Find all xsd imports
        for importXsdElement in wsdlContent.iter('{http://www.w3.org/2001/XMLSchema}import'):
            foundFileName = getReferencedXsdFromImportElement(importXsdElement)
            if foundFileName not in schemaList:
                schemaList.append(foundFileName)
        
        # Find all wsdl imports
        for importWsdlElement in wsdlContent.iter('{http://schemas.xmlsoap.org/wsdl/}import'):
            foundFileName = getReferencedXsdFromImportElement(importWsdlElement)
            if foundFileName not in schemaList:
                schemaList.append(foundFileName)
        # Find all includes
        for includeElement in wsdlContent.iter('{http://www.w3.org/2001/XMLSchema}include'):
            foundFileName = getReferencedXsdFromImportElement(includeElement)
            if foundFileName not in schemaList:
                schemaList.append(foundFileName)
                
        # If an error already occurred during scan stop scanning
        if Vars.exitValue > 90 :
            return []
            
        missingFiles = []
        
        if schemaList:
            for foundFile in schemaList:
                # Ignore remote file referencing
                if foundFile.startswith('http') :
                    continue
                fileNameOnFileSystem = findXSDFileNameInConfigByPath(foundFile)
                if fileNameOnFileSystem != '':
                    missingFiles.extend([XMLFilePath, mapReferenceHierarchyOfWSDLFile(fileNameOnFileSystem)])
                else:
                    Vars.exitValue = 1
                    missingFiles.extend([XMLFilePath,[foundFile]])
        else:
            missingFiles.extend([XMLFilePath,[]])
        return missingFiles
    # The refrenced wsdl is not found
    except IOError as notFoundError:
        print(XMLFilePath + ' was not found on filesystem')
        # Couldn't parse the WSDL, either non xml or file doesnt exist
        # return it as part of the list
        return [ XMLFilePath ]
    except xml.etree.ElementTree.ParseError as parseError:
        # The referenced file was found but is invalid XML
        # Exit app
        Vars.error_message = XMLFilePath + ' is invalid XML file, at line %s column %s' % (parseError.position)
        Vars.exitValue = 99

mapReferenceHierarchyOfWSDLFile.scannedFiles = []
Vars.exitValue = 0
Vars.error_message = ""

# Receive a tree structure of XSD\WSDL references and prints out only the missing referenced file and their referencing files to an output file
# Eg. Input: ['references.wsdl', ['import.xsd', []], 'references.wsdl', ['my/import22.xsd'], 'references.wsdl', ['include.xsd', []]]
# This input means that only 'my/import22.xsd' is missing and therfore only that should be printed to an external file
def missingfileReferencesToFlatFile( referenceTree ):
    payload = ""
    for reference in referenceTree:
        # For each node in the tree, check that it is not a string yet it is a collection
        if isinstance(reference, collections.abc.Iterable) and not isinstance(reference, str) :
            # If the collection contains one object only, it is a missing XSD file
            if len(reference) == 1:
                # Source file will be always prior in list to the referenced file
                sourceFile = referenceTree[referenceTree.index(reference) - 1]
                destFile = reference[0]
                # If file has relative path, make it absolute using the path of the wsdl
                if not destFile.startswith('local:///'):
                    wsdlDirNameInDatapower = config["original_start_wsdl_dp_path"].rsplit('/',1)[0] + '/'
                    destFile = wsdlDirNameInDatapower + destFile
                
                missingfileReferencesToFlatFile.missingFileCount += 1
                payload += ("source_requesting_file.%d=local:///" + sourceFile + "\nfile_to_download.%d=" + destFile + "\n")%(missingfileReferencesToFlatFile.missingFileCount - 1,missingfileReferencesToFlatFile.missingFileCount - 1)
            # Otherwise, keep scanning the tree recursively
            else :
                payload += missingfileReferencesToFlatFile(reference)
    return payload
    
missingfileReferencesToFlatFile.missingFileCount = 0;

def findXSDFileNameInConfigByPath(pathToFind):
    mappedXsd = int(config["total_original_schema_files"])
    if not pathToFind.startswith('local:///'):
        wsdlDirNameInDatapower = config["original_start_wsdl_dp_path"].rsplit('/',1)[0] + '/'
        pathToFind = wsdlDirNameInDatapower + pathToFind
    
    for n in range(0,mappedXsd):
        dppath = config["original_schema_file.%d.dp_path" % (n)]
        if config["original_schema_file.%d.dp_path" % (n)] == pathToFind:
            path = config["original_schema_file.%d.name" % (n)]
            return path
    return ''

# Loads a properties file into dictionary
def load_properties(filepath, sep='=', comment_char='#'):
    """
    Read the file passed as parameter as a properties file.
    """
    props = {}
    with open(filepath, "rt") as f:
        for line in f:
            l = line.strip()
            if l and not l.startswith(comment_char):
                key_value = l.split(sep)
                key = key_value[0].strip()
                value = sep.join(key_value[1:]).strip().strip('"') 
                props[key] = value 
    return props

# The input file should be in the same directory as the script
config = load_properties('input.props')

# Load and parse WSDL file, and make a tree out of the referenced fiels
hierarchyReferencedXSDs = mapReferenceHierarchyOfWSDLFile(config["original_start_wsdl_name"])

# Find the missing files in the reference hierarcy
missingFiles = missingfileReferencesToFlatFile(hierarchyReferencedXSDs)

if len(missingFiles) > 0:
    Vars.exitValue = 1

# Build the payload
payload = "completion_code=%d\nerror_message=%s\n%s" % (Vars.exitValue, Vars.error_message, missingFiles)
print(payload)
# Save the payload into file so DPOD can read the output
f = open('completion-%s.props'%(config['iteration']), 'w')
f.write(payload)
sys.exit(Vars.exitValue)
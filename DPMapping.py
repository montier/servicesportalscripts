#   
# 	Licensed to MonTier Software (2015) LTD under one or more contributor
#   license agreements. See the NOTICE file distributed with
#   this work for additional information regarding copyright
#   ownership. MonTier Software (2015) LTD licenses this file to you under
#   the Apache License, Version 2.0 (the "License"); you may
#   not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#  
#      http://www.apache.org/licenses/LICENSE-2.0
#  
#   Unless required by applicable law or agreed to in writing,
#   software distributed under the License is distributed on an
#   "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
#   KIND, either express or implied.  See the License for the
#   specific language governing permissions and limitations
#   under the License.
#
 
class DPMapping(object):
    """A Mapping of xsd in datapower"""
 
    def __init__(self,pReferencedByDpPath, pDpPath, pLocalPath, pNameSpace):
        self.referencedByDpPath = pReferencedByDpPath
        self.dpPath = pDpPath 
        self.localPath = pLocalPath
        self.nameSpace = pNameSpace
        if not self.nameSpace or self.nameSpace == '':
            raise ValueError("Namespace can't be empty")

    def __str__(self):
        return '(DpPath: ' + self.dpPath + ' ,LocalPath: ' + self.localPath + ' ,NameSpace: ' + self.nameSpace + ' ,ReferencedBy: ' + self.referencedByDpPath + ')\n'
    def __unicode__(self):
        return self.__str__()
    def __repr__(self):
        return self.__str__()